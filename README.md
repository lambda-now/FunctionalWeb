# Modern Haskell Web Programming

This is a book on learning Haskell for programmers who know another
language but would like to learn Haskell. It takes a very hands-on
approach. Each section has a working and simple example, and a series
of exercises. The book defers discussions of theory to other works.

## Downloading the Text

The latest text will always be available through our
[GitLab repository][GitLab repository].

[GitLab repository]: https://gitlab.com/lambda-now/HaskellWeb/blob/HaskellWeb/HaskellWeb.pdf

## Cloning the Repositories

We don't use the problematic "master/slave" terminology, like
many [other projects][master]. Our main branch is currently called
`HaskellWeb`.

To clone:

    git clone --recursive https://gitlab.com/misandrist/HaskellWeb.git
    cd HaskellWeb

## Building and previewing

### LaTeX

This book is written in [LaTeX][latex], a high-quality typesetting
system, especially suitable for writing technical documents and books
such as this one.

Preview the changes you make to the LaTeX files, you will have to compile
the source files into a viewable format viz., dvi, or PDF.

The best way to install LaTeX on your system is:

* [MixTeX][miktex] on Windows
* [TexLive][texlive] on Mac OS X and Linux


### Building the PDF

Once you have a functioning LaTeX system installed, you can kick-off the
compilation process with `latexmk`

    latexmk -xelatex HaskellWeb.tex

If everything goes smoothly, you will see a
new [HaskellWeb.pdf][book].

[master]: https://github.com/django/django/pull/2692 "Master/Slave"
[latex]: http://www.latex-project.org/
[miktex]: http://miktex.org/
[texlive]: https://www.tug.org/texlive/
[book]: HaskellWeb.pdf
